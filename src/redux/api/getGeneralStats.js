const getGeneralStats = () => 
  fetch(`stats/`, {
      method: `GET`,
      headers: new Headers({
        'Content-Type': 'application/json',
    }),
  })
    .then(response => response.json())
    .then(res => res, err => err)
    .catch(error => console.log('Error w GET general stats: ', JSON.stringify(error)));

export default getGeneralStats;
