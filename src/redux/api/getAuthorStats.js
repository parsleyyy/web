const getAuthorStats = (author) => 
  fetch(`stats/${author}`, {
      method: `GET`,
      headers: new Headers({
        'Content-Type': 'application/json',
    }),
  })
    .then(response => response.json())
    .then(res => res, err => err)
    .catch(error => console.log('Error w GET author stats: ', JSON.stringify(error)));

export default getAuthorStats;
