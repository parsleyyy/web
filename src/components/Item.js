import React from 'react';
import PropTypes from 'prop-types';

const Item = (props) => {
  const { author, handleInputChange } = props;
  
  return (
    <li 
      className={author.checked ? 'marked' : 'unmarked' }
    >
      <label>
        <input
          name={author.name}
          type="checkbox"
          checked={author.checked}
          onChange={() => handleInputChange(author.id === 'all' ? 'all' : author)}
        />
      </label>
      {author.name}
    </li>
  );
};

Item.propTypes = {
  author: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
  }),
  handleInputChange: PropTypes.func,
};

Item.defaultProps = {
  author: PropTypes.shape({
    id: '',
    name: '',
    checked: false,
  }),
  handleInputChange: undefined,
};

export default Item;
