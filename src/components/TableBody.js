import React from 'react';
import PropTypes from 'prop-types';

const TableBody = (props) => (
  <tbody>
    {props.data.map((row, index) => (
        <tr key={`${row.name}`}>
          <td key={`i${row.name}`}>{index + 1}</td>
          <td key={`name${row.name}`}>{row.name}</td>
          <td key={row.count}>{row.count}</td>
        </tr>
      ))
    }
  </tbody>
);

TableBody.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      count: PropTypes.number,
    })),
  };
  
TableBody.defaultProps = {
  data: PropTypes.arrayOf(PropTypes.shape({
    name: '',
    count: 0,
  })),
};

export default TableBody;
