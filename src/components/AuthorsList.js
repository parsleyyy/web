import React from 'react';
import Item from './Item';
import PropTypes from 'prop-types';

const AuthorsList = (props) => {
  const { authorsData, generalData, handleInputChange, listOpen, handleListOpen } = props;

  const authors = authorsData.map((author) => {
    const { id } = author;
    return (
      <Item
        key={id}
        author={author}
        handleInputChange={handleInputChange}
      />
    );
  });

  return (
    <div className="list">
      <button className="collapsible" onClick={() => handleListOpen()}>Authors List</button>
      {listOpen ? 
        <div className="listContent" >
          <ul>
            {authors} 
            {generalData.keywords ?
              <Item 
                key={generalData.id}
                author={generalData}
                handleInputChange={handleInputChange}
              />
              :
              <p>Brak danych</p>
            }
          </ul>
        </div> :
        <></>
      } 
    </div>
  );
};

AuthorsList.propTypes = {
  listOpen: PropTypes.bool,
  authorsData: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
  })),
  generalData: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
  }),
  handleInputChange: PropTypes.func,
  handleListOpen: PropTypes.func,
};

AuthorsList.defaultProps = {
  listOpen: false,
  authorsData: [],
  generalData: {},
  handleInputChange: undefined,
  handleListOpen: undefined,
};

export default AuthorsList;
