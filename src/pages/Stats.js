import React from 'react';
import { connect } from 'react-redux';
import reduxActions from '../redux/actions/actions';
// import components
import AuthorsList from '../components/AuthorsList';
import Blocks from '../components/Blocks';
import StatsTable from '../components/StatsTable';
import ErrorMessage from '../components/ErrorMessage';

class Stats extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listOpen: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleListOpen = this.handleListOpen.bind(this);
  };

  componentDidMount() {
    const { dispatch } = this.props;
    // fetching authors and compose structure of state.authorsData and state.generalData
    dispatch(reduxActions.getAuthorsRequest());
  };

  // dispatching action on checkbox state change
  handleInputChange = (author) => {
    const { dispatch, generalData } = this.props;

    author === "all" ? dispatch(reduxActions.getStatsRequest({...generalData})) : dispatch(reduxActions.getAuthorStatsRequest(author));
  };

  // open/close colapsible list
  handleListOpen() {
    this.setState(prevState => ({
      listOpen: !prevState.listOpen,
    }));
  };

  render() {
    const { authorsData, generalData, error } = this.props;

    // sorting data object by its name property (abstractiong author's surname)
    const sortedData = authorsData.sort((a, b) => {
      if (a.name.substring(a.name.indexOf(" ") + 1) > b.name.substring(b.name.indexOf(" ") + 1)) return 1;
      if ((b.name.substring(b.name.indexOf(" ") + 1) > a.name.substring(a.name.indexOf(" ") + 1))) return -1;
      return 0;
    })
   
    let fuseStats = [];
    
    // when more then one author is checked: sum of words of several authors
    authorsData.filter(author => author.checked)
      .forEach(author => author.keywords
        .forEach(keyword => fuseStats.findIndex(obj => obj.name === keyword.name) === -1 ? 
          fuseStats.push({ name: keyword.name, count: keyword.count }) : 
          fuseStats[fuseStats.findIndex(obj => obj.name === keyword.name)].count += keyword.count
      ));

    // sorting the most common words
    const sortedFuseStats = fuseStats.sort((a, b) => b.count - a.count); //fuseStats.sort((a, b) => a.name < b.name ? 1 : b.name < a.name ? -1 : 0);
    
    // preparing stats to showing
    const statsToShow = generalData.checked ? generalData.keywords : sortedFuseStats;

    return (
      <div className="App">
        <ErrorMessage error={error} />
        <AuthorsList 
          authorsData={sortedData} 
          generalData={generalData}
          handleInputChange={this.handleInputChange}
          listOpen={this.state.listOpen}
          handleListOpen={this.handleListOpen}
        />
        <Blocks
          allChecked={generalData.checked}
          authorsData={sortedData}
          handleInputChange={this.handleInputChange}
        />
        <StatsTable
          data={statsToShow}
        />  
      </div>
    );
  };
};

const mapStateToProps = state => ({
  authorsData: state.data.authorsData,
  generalData: state.data.generalData,
  error: state.data.error,
});

const ConnectedApp = connect(mapStateToProps)(Stats);

export default ConnectedApp;
