import {
  put,
  takeEvery,
  call,
  all,
} from 'redux-saga/effects';
import actionTypes from '../actions/actionTypes';
import getAuthors from '../api/getAuthors';
import getGeneralStats from '../api/getGeneralStats';
import getAuthorStats from '../api/getAuthorStats';

// GET AUTHORS FROM API
export function* getAuthorsAsync() {
  const res = yield call(getAuthors);

  if (res instanceof Error) {
    yield put({
      type: actionTypes.GET_AUTHORS_FAILURE,
      error: res,
    });
  } else {
    const data = [];
    const generalData = {
      name: 'all',
      id: 'all',
      checked: false,
      keywords: [],
    };

    Object.entries(res).map(([key, value]) => data.push({
      name: value,
      id: key,
      checked: false,
      keywords: [],
    }));

    yield put({
      type: actionTypes.GET_AUTHORS_SUCCESS,
      authorsData: data,
      generalData,
    });
  };
};

// GET GENERAL STATS FROM API
export function* getGeneralStatsAsync(action) {
  const { generalData } = action;
  const res = yield call(getGeneralStats);

  if (res instanceof Error) {
    yield put({
      type: actionTypes.GET_STATS_FAILURE,
      error: res,
    });
  } else {
    generalData.checked = !generalData.checked;
    generalData.keywords = Object.entries(res).map(([key, value]) => ({ name: key, count: value }));

    yield put({
      type: actionTypes.GET_STATS_SUCCESS,
      generalData,
    });
  };
};

// GET SINGLE AUTHOR STATS FROM API
export function* getAuthorStatsAsync(action) {
  const { author } = action;
  const res = yield call(getAuthorStats, author.id);
  

  if (res instanceof Error) {
    yield put({
      type: actionTypes.GET_AUTHOR_STATS_FAILURE,
      error: res,
    });
  } else {
    author.keywords = Object.entries(res).map(([key, value]) => ({ name: key, count: value }));
    author.checked = !author.checked;
    
    yield put({
      type: actionTypes.GET_AUTHOR_STATS_SUCCESS,
      author,
    });
  };
};

export function* rootSaga() {
  yield all([
    takeEvery(actionTypes.GET_AUTHORS_REQUEST, getAuthorsAsync),
    takeEvery(actionTypes.GET_STATS_REQUEST, getGeneralStatsAsync),
    takeEvery(actionTypes.GET_AUTHOR_STATS_REQUEST, getAuthorStatsAsync),
  ]);
}
