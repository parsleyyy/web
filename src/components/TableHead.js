import React from 'react';

const renderHeadingRow = (head) => (
  <th key={head}>{head}</th>
);

const TableHead = () => {
  const headings = [
    "",
    "Word",
    "Count",
  ];

  return (
    <thead>
      <tr>
        {headings.map(renderHeadingRow)}
      </tr>
    </thead>
  );
};

export default TableHead;
