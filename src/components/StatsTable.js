import React from 'react';
import PropTypes from 'prop-types';
import TableHead from './TableHead';
import TableBody from './TableBody';

const StatsTable = (props) => (
  <table border="1" >
    <TableHead 
      key="thaed" 
    /> 
    <TableBody
      key="tbody"
      data={props.data}
    />
  </table>
);

StatsTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    count: PropTypes.number,
  })),
};

StatsTable.defaultProps = {
  data: PropTypes.arrayOf(PropTypes.shape({
    name: '',
    count: 0,
  })),
};

export default StatsTable;
