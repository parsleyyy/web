import React from 'react';
import PropTypes from 'prop-types';

const Blocks = (props) => {
  const { allChecked, authorsData, handleInputChange } = props;
  
  return allChecked ? <button className="blocks" onClick={() => handleInputChange('all')} >All X</button> : 
    authorsData.map((author) => author.checked ? 
      <button key={author.id} className="blocks" onClick={() => handleInputChange(author)} >{`${author.name} X`}</button> : false);
};

Blocks.propTypes = {
  allChecked: PropTypes.bool,
  authorsData: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
  })),
  handleInputChange: PropTypes.func,
};

Blocks.defaultProps = {
  allChecked: false,
  handleInputChange: undefined,
  authorsData: [],
};

export default Blocks;
