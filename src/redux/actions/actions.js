import actionTypes from './actionTypes';

// API - GET AUTHOR'S NAMES
const getAuthorsRequest = () => ({
  type: actionTypes.GET_AUTHORS_REQUEST
});

const getAuthorsFailure = () => ({
  type: actionTypes.GET_AUTHORS_FAILURE,
});

const getAuthorsSuccess = () => ({
  type: actionTypes.GET_AUTHORS_SUCCESS,
});

// API - GET GENERAL STATS
const getStatsRequest = (generalData) => ({
  type: actionTypes.GET_STATS_REQUEST,
  generalData,
});

const getStatsFailure = () => ({
  type: actionTypes.GET_STATS_FAILURE,
});

const getStatsSuccess = () => ({
  type: actionTypes.GET_STATS_SUCCESS,
});

// API - GET AUTHORS STATS
const getAuthorStatsRequest = (author) => ({
  type: actionTypes.GET_AUTHOR_STATS_REQUEST,
  author,
});

const getAuthorStatsFailure = () => ({
  type: actionTypes.GET_AUTHOR_STATS_FAILURE,
});

const getAuthorStatsSuccess = () => ({
  type: actionTypes.GET_AUTHOR_STATS_SUCCESS,
});

export default {
  getAuthorsFailure,
  getAuthorsRequest,
  getAuthorsSuccess,
  getStatsFailure,
  getStatsRequest,
  getStatsSuccess,
  getAuthorStatsFailure,
  getAuthorStatsRequest,
  getAuthorStatsSuccess,
};
