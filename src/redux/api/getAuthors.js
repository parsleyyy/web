const getAuthors = () => 
  fetch(`authors/`, {
      method: `GET`,
      headers: new Headers({
        'Content-Type': 'application/json',
    }),
  })
    .then(response => response.json())
    .then(res => res, err => err)
    .catch(error => console.log('Error w GET authors: ', JSON.stringify(error)));

export default getAuthors;
