## Opis wykonanego zadania

W ramach zadania przedstawione zostały dane wystawione przez REST API. W tabeli wyświetlić można 10 najczęściej występujących słów z ich liczebnością: ogólem, per autor oraz jako suma wybranych autorów. W liście rozwijanej mamy do wyboru danych autorów oraz opcję *all*. W celu wybrania danej opcji należy zaznaczyć odpowiedni checkbox. Możliwe jest zaznaczenie kilku autorów jednocześnie, co skutkować będzie wyświetleniem sumy statystyk wybranych autorów. 

Dodatkowo, wybrani przez użytkownika autorzy bądż opcja *all* wyświetlana jest w badge'ach, które można usunąć klikając na nie (niewyświetlane są już dane usuniętego autora/autorów).

W kolumnie *Words* pojawiają się słowa najczęściej używane przez autora/autorów, posegregowane według liczby użycia. W kolumnie *Count* wyświetlana jest liczba użycia danego słowa dla autora/autorów.

Aplikacja wystawiona jest pod adresem *localhost:3000/stats/*.


## Zastosowane technologie

Kod napisany jest w React w wersji 16.8.6. 
Zależności aplikacji umieszczone są w pliku package.json (wykorzystanie npm).
Za pomocą Redux "trzymane" są wewnętrzne stany aplikacji - połączenie z Reactem za pomocą react-redux.
Zapis danych z API odbywa się za pomocą redux-saga (1.0.5), następnie dane przekazywane są do Reduxa.
Routing stworzony jest w oparciu o react-router 4.3.1.


## Uruchomienie

Będąc w folderze web:

**docker-compose up -d**

A następnie:

**npm start**


## Problemy

W projekcie największą trudność sprawiło mi wykorzystanie Dockera. W repozytorium zawarłam plik docker-compose.yml, lecz nie udało mi się sprzęgnąć wszystkich obrazów w powodu błędu: "Invalid HTTP HOST header: 'api:8080'. You may need to add 'api' to ALLOWED HOSTS". Używając *curl*'a udało mi się wykonywać poprawne zapytania do API z kontenera, w którym była aplikacja webowa podmieniając HTTP hostheader na localhost, ale (jak wynika z dokumentacji React'a) funkcja fetch nie daje takich możliwości. 
Wadliwy kod znajduje się w commicie nr 5436d0da.

W ostanim commicie nr 38bbd929 kod zmodyfikowany jest w sposób, w którym omijam część dockerową poprzez ustawienie w package.json *"proxy": "http://localhost:8080"* (localhost zamiast obraz api) oraz usuniecie obrazu web w docker-compose.yml.