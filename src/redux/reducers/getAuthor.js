import actionTypes from '../actions/actionTypes';

const initialStateAuthors = {
  authorsData: [],
  generalData: {},
  authorsLoading: false,
  generalStatsLoading: false,
};

const reducerAuthors = (state = initialStateAuthors, action = {}) => {
  switch (action.type) {
    case actionTypes.GET_AUTHORS_REQUEST:
      return { ...state, authorsLoading: true, error: null };
    case actionTypes.GET_AUTHORS_SUCCESS:
      return { ...state, authorsLoading: false, authorsData: action.authorsData, generalData: action.generalData};
    case actionTypes.GET_AUTHORS_FAILURE:
      return { ...state, authorsData: [], generalData: [], authorsLoading: false, error: action.error };
    case actionTypes.GET_AUTHOR_STATS_REQUEST:
      return { ...state, authorStatsLoading: true, error: null };
    case actionTypes.GET_AUTHOR_STATS_SUCCESS:
      return { 
        ...state,
        authorsData: [
          ...state.authorsData.filter(author => author.id !== action.author.id),
          action.author,
        ],
        authorStatsLoading: false,
      };
    case actionTypes.GET_AUTHOR_STATS_FAILURE:
      return { ...state, authorStatsLoading: false, error: action.error };
    case actionTypes.GET_STATS_REQUEST:
      return { ...state, generalStatsLoading: true, error: null };
    case actionTypes.GET_STATS_SUCCESS:
      return { ...state, generalData: action.generalData, generalStatsLoading: false };
    case actionTypes.GET_STATS_FAILURE:
      return { ...state, generalStatsLoading: false, error: action.error };
    default:
      return state;
  };
};

export default reducerAuthors;
