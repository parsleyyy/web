import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = (props) => (
  props.error ? <div className="error" >BŁAD PODCZAS POBIERANIA DANYCH</div> : <></>
);

ErrorMessage.propTypes = {
  error: PropTypes.instanceOf(Error),
};

ErrorMessage.defaultProps = {
  error: undefined,
};

export default  ErrorMessage;
