import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history'
import { rootSaga } from '../sagas/index';
import reducerAuthors from './getAuthor';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

const rootReducer = (history) => combineReducers({
  router: connectRouter(history),
  data: reducerAuthors,
});

export const history = createBrowserHistory();

export default createStore(
  rootReducer(history),
  applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware, 
    loggerMiddleware
  ),
);

sagaMiddleware.run(rootSaga);
