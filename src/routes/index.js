import React from 'react';
import { Route, Switch } from 'react-router';
import Stats from '../pages/Stats';
import NoMatch from '../pages/NoMatch';

const routes = (
  <div>
    <Switch>
      <Route path="/stats" component={() => <Stats />} />
      <Route component={NoMatch} />
    </Switch>
  </div>
)

export default routes;
